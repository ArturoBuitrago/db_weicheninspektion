namespace DVXML { 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Globalization;
using System.Runtime.Serialization;



//[DataContract(Name = "game", Namespace = "")]
[System.Serializable]
public class Point
{
    public string name;
    public Dictionary<string, Formula> formulasD = new Dictionary<string, Formula>(); //id, Description
    public Dictionary<string, Description> descriptionsD = new Dictionary<string, Description>(); //id, Description
    public Dictionary<string, string> commentsD = new Dictionary<string, string>(); //id, Description
    public Dictionary<string, List<MeasurementPoint>> measurementPoints = new Dictionary<string, List<MeasurementPoint>>(); //dictionary to map subblocks to corresponding measurepoints
    //public List<string> measurementNames = new List<string>();

    //public void generateOrderedMeasurements()
    //{

    //}
}

public class FormulaDictionary
    {
        string key;
        Formula formula;
    }

    [System.Serializable]
    public class MeasurementPoint
{
    public string name;
    public List<Measurement> measurements = new List<Measurement>();
    public string POI;
        //public MeasurementPoint(string subblock)
        //{
        //    this.subblock = subblock;
        //}
    }

[System.Serializable]
public class Measurement : MeasurementBase
{
    public string descriptionId;
    public Calculation calculation;
    public string sr100Minus;
    public string sr100Plus;
    public string srlimMinus;
    public string srlimPlus;
    public string grenzwertMinus;
    public string grenzwertPlus;
}

[System.Serializable]
public class Calculation : MeasurementBase
{
    public string formulaId;
    public string sr100;
    public string srlim;
    public string grenzwert;
}

public class MeasurementBase
{
    public string name;
    public string commentID;
    public string fortsetzung;
    public string sollwert;
    
}

[System.Serializable]
    public class Description
{
    public string shortDes;
    public string longDes;
    public Description(string shortDes, string longDes)
    {
        this.shortDes = shortDes;
        this.longDes = longDes;
    }
}

    [System.Serializable]
    public class Formula
{
    public string name;
    public string formula;
    public Formula(string name, string formula)
    {
        this.name = name;
        this.formula = formula;
    }
}
public class XMLPointReader  // the Class
{
    public TextAsset GameAsset;



    void Start()
    {
        ReadXMLToPointClass();
    }

        public Point ReadXMLToPointClass()
        {
            return ReadXMLToPointClass(GameAsset);
        }
        public Point ReadXMLToPointClass(TextAsset XML)
    {
        Point point = new Point();
        XmlDocument xmlDoc = new XmlDocument(); // xmlDoc is the new xml document.
        xmlDoc.LoadXml(XML.text); // load the file.
        XmlNodeList formulas = xmlDoc.GetElementsByTagName("data:formula"); // array of the level nodes.

        point.name = xmlDoc.GetElementsByTagName("point")[0].Attributes["name"].Value;

        foreach (XmlNode formula in formulas)
        {
            point.formulasD.Add(formula.Attributes["id"].Value, new Formula(formula.Attributes["name"].Value, formula.InnerXml.ToString()));
        }

        XmlNodeList descriptions = xmlDoc.GetElementsByTagName("data:description");

        foreach (XmlNode description in descriptions)
        {
            string shortD = "";
            string longD = "";
            XmlNodeList decriptionSubElements = description.ChildNodes;

            foreach (XmlNode decriptionSubElement in decriptionSubElements)
            {
                switch (decriptionSubElement.Name)
                {
                    case "short":
                        shortD = decriptionSubElement.InnerXml.ToString();
                        break;
                    case "long":
                        longD = decriptionSubElement.InnerXml.ToString();
                        break;
                }
            }

            point.descriptionsD.Add(description.Attributes["id"].Value, new Description(shortD, longD));

        }

        XmlNodeList comments = xmlDoc.GetElementsByTagName("data:comment"); // array of the level nodes.

        foreach (XmlNode comment in comments)
        {
            point.commentsD.Add(comment.Attributes["id"].Value, comment.InnerXml.ToString());

        }
        XmlNodeList measurementPoints = xmlDoc.GetElementsByTagName("measurementpoints");

        foreach (XmlNode measurementPoint in measurementPoints)
        {


            XmlNodeList subblocks = measurementPoint.ChildNodes;

            foreach (XmlNode subBlock in subblocks)
            {


                XmlNodeList mps = subBlock.ChildNodes;
                List<MeasurementPoint> measurementPointList = new List<MeasurementPoint>();
                foreach (XmlNode mp in mps)
                {
                    MeasurementPoint measurementPointC = new MeasurementPoint();
                    measurementPointList.Add(measurementPointC);
                    point.measurementPoints[subBlock.Attributes["name"].Value] = measurementPointList;
                    measurementPointC.name = mp.Attributes["name"].Value;
                    measurementPointC.POI = mp.Attributes["POI"].Value;
                    XmlNodeList measurements = mp.ChildNodes;

                    foreach (XmlNode measurement in measurements)
                    {
                        Measurement measurementC = new Measurement();
                        measurementPointC.measurements.Add(measurementC);
                        XmlNodeList measurementSubElements = measurement.ChildNodes;
                        measurementC.name = measurement.Attributes["name"].Value;
                            
                        foreach (XmlNode measurementSubElement in measurementSubElements)
                        {
                            switch (measurementSubElement.Name)
                            {
                                case "ref:description":
                                    measurementC.descriptionId = measurementSubElement.Attributes["ref"].Value;
                                    Debug.Log("Measure Description ID " + measurementC.descriptionId + " Description " + point.descriptionsD[measurementC.descriptionId].shortDes + " " + point.descriptionsD[measurementC.descriptionId].longDes); //Debug
                                    break;
                                case "fortsetzung":
                                    measurementC.fortsetzung = measurementSubElement.InnerXml;
                                    break;
                                case "sollwert":
                                    measurementC.sollwert = measurementSubElement.InnerXml;
                                    break;
                                case "sr100":
                                    string[] sr100MinusPlus = SplitValues(measurementSubElement.InnerText);
                                    measurementC.sr100Minus = sr100MinusPlus[0];
                                    measurementC.sr100Plus = sr100MinusPlus[1];
                                    break;
                                case "srlim":
                                    string[] srlimMinusPlus = SplitValues(measurementSubElement.InnerText);
                                    measurementC.srlimMinus = srlimMinusPlus[0];
                                    measurementC.srlimPlus = srlimMinusPlus[1];
                                    break;
                                case "grenzwert":
                                    string[] grenzwertMinusPlus = SplitValues(measurementSubElement.InnerText);
                                    measurementC.grenzwertMinus = grenzwertMinusPlus[0];
                                    measurementC.grenzwertPlus = grenzwertMinusPlus[1];
                                    break;
                                case "calculation":
                                    XmlNodeList calculationSubNodes = measurementSubElement.ChildNodes;
                                    Calculation calculationC = new Calculation();
                                    measurementC.calculation = calculationC;
                                    measurementC.calculation.name = measurementSubElement.Attributes["name"].Value;
                                        foreach (XmlNode calculationSubNode in calculationSubNodes)
                                    {
                                        switch (calculationSubNode.Name)
                                        {
                                            case "ref:formula":
                                                calculationC.formulaId = calculationSubNode.Attributes["ref"].Value;
                                                Debug.Log("Measure Formula ID " + calculationC.formulaId + " Formula " + point.formulasD[calculationC.formulaId].formula + " " + point.formulasD[calculationC.formulaId].formula); //Debug
                                                break;
                                            case "fortsetzung":
                                                calculationC.fortsetzung = calculationSubNode.InnerXml;
                                                break;
                                            case "sollwert":
                                                calculationC.sollwert = calculationSubNode.InnerXml;
                                                break;
                                            case "sr100":
                                                calculationC.sr100 = calculationSubNode.InnerText;
                                                break;
                                            case "srlim":
                                                calculationC.srlim = calculationSubNode.InnerText;
                                                break;
                                            case "grenzwert":
                                                calculationC.grenzwert = calculationSubNode.InnerText;
                                                break;
                                            case "ref:comment":
                                                calculationC.commentID = calculationSubNode.Attributes["ref"].Value;
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        }

        return point;
    }

    private string[] SplitValues(string values)
    {
        return values.Split(',');
    }

}
    }


