﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualFeedback : MonoBehaviour {

    [SerializeField]
    public bool isFirstSwitch = false;
    public GameObject track;
    public GameObject ring;
    public GameObject innerRing;
    public GameObject verticalStripe;
    public GameObject infoBox;
    public GameObject infoBoxHighlight;
    public GameObject nextTie;
    public GameObject moveOnButton;

    GameObject ringBase;
    GameObject ringRawImage;
    List<GameObject> ringAllElements = new List<GameObject>();

    GameObject infoBoxBackground;
    GameObject infoBoxText1;
    GameObject infoBoxText2;
    GameObject moveOnButtonBackground;
    GameObject moveOnButtonHighlight;
    GameObject NextRawImage;

    List<GameObject> infoBoxAllElements = new List<GameObject>();

    Color red = Color.green;
    Color yellow = Color.yellow;
    Color green = Color.green;

    private float distance;
    private float threshold = 2.0f;
    private float changeArea = 1.0f;
    private Camera camera;

    float newAlpha;
    float currentAlpha;

    // Use this for initialization
    void Start () {
        
        camera = Camera.main;

        ringBase = ring.transform.Find("Base").gameObject;
        ringRawImage = ring.transform.Find("RawImage").gameObject;

        ringAllElements.Add(ringBase);
        ringAllElements.Add(ringRawImage);
        ringAllElements.Add(innerRing);

        infoBoxBackground = infoBox.transform.Find("BackgroundImage").gameObject;
        infoBoxText1 = infoBox.transform.Find("TextTODO").gameObject;
        infoBoxText2 = infoBox.transform.Find("TextTechData").gameObject;
        moveOnButtonBackground = moveOnButton.transform.Find("BackgroundImage").gameObject;
        moveOnButtonHighlight = moveOnButton.transform.Find("HighlightImage").gameObject;
        NextRawImage = moveOnButton.transform.Find("Next").gameObject;
        infoBoxHighlight = infoBox.transform.Find("HighlightImage").gameObject;

        infoBoxAllElements.Add(infoBoxBackground);
        infoBoxAllElements.Add(infoBoxText1);
        infoBoxAllElements.Add(infoBoxText2);
        infoBoxAllElements.Add(infoBoxHighlight);
        infoBoxAllElements.Add(moveOnButtonBackground);
        infoBoxAllElements.Add(moveOnButtonHighlight);
        infoBoxAllElements.Add(NextRawImage);

        if (isFirstSwitch)
        {
            MarkAsActive();
        }
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(camera.gameObject.transform.position, b: this.gameObject.transform.position);

        UpdateTransparencyInfoBox(distance);
        UpdateTransparencyRing(distance);

        //Make objects look at camera
        UpdateDirection();

    }

    private void UpdateDirection()
    {
        //Ring
        /*foreach (GameObject obj in ringAllElements)
        {
            obj.transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward,
            camera.transform.rotation * Vector3.up);
        }*/
        //InfoBox
        //infoBox.transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, camera.transform.rotation * Vector3.up);

    }

    private void UpdateTransparencyRing(float distance)
    {
        if (distance < threshold)
        {
            newAlpha = 0f;
        }
        else if (distance > threshold + changeArea)
        {
            newAlpha = 1.0f;
        }
        else
        {
            newAlpha = (distance - threshold) / (changeArea);
        }

        verticalStripe.GetComponent<Image>().color = new Color(verticalStripe.GetComponent<Image>().color[0], verticalStripe.GetComponent<Image>().color[1], verticalStripe.GetComponent<Image>().color[2], newAlpha);

        foreach (GameObject obj in ringAllElements)
        {

            Image objimage = obj.GetComponent<Image>();
            if (objimage == null)
            {
                RawImage rawobjimage = obj.GetComponent<RawImage>();
                rawobjimage.color = new Color(rawobjimage.color[0], rawobjimage.color[1], rawobjimage.color[2], newAlpha);
                continue;
            }
            objimage.color = new Color(objimage.color[0], objimage.color[1], objimage.color[2], newAlpha);

        }
    }

    private void UpdateTransparencyInfoBox(float distance)
    {
        if(distance < threshold)
        {
            newAlpha = 1.0f;
        } else if (distance > threshold + changeArea)
        {
            newAlpha = 0f;
        } else
        {
            newAlpha = 1- (distance - threshold) / (changeArea);
        }

        //Get the appropriate elements
        foreach (GameObject obj in infoBoxAllElements)
        {

            Image objimage = obj.GetComponent<Image>();
            if (objimage == null)
            {
                RawImage rawobjimage = obj.GetComponent<RawImage>();
                if (rawobjimage == null)
                {
                    Text text = obj.GetComponent<Text>();
                    text.color = new Color(text.color[0], text.color[1], text.color[2], newAlpha);
                    continue;
                }
                rawobjimage.color = new Color(rawobjimage.color[0], rawobjimage.color[1], rawobjimage.color[2], newAlpha);
                continue;
            }
            objimage.color = new Color(objimage.color[0], objimage.color[1], objimage.color[2], newAlpha);

        }

    }

    private void ChangeColor(Color color)
    {
        //track
        
        //track.GetComponent<Image>().color = color;

        //vertical strpie
        verticalStripe.GetComponent<Image>().color = color;

        innerRing.GetComponent<Image>().color = color;

        infoBoxHighlight.GetComponent<Image>().color = color;

        moveOnButtonBackground.GetComponent<Image>().color = color;


        //throw new NotImplementedException();
    }

    public void MarkAsCompleted()
    {
        ChangeColor(green);
        ringRawImage.GetComponent<ChangeIcon>().ChangeIconTo("check");
        if(!nextTie == default(GameObject))
        {
            nextTie.GetComponent<VisualFeedback>().MarkAsActive();
        }
        
    }

    public void MarkAsActive()
    {
        ChangeColor(yellow);
        ringRawImage.GetComponent<ChangeIcon>().ChangeIconTo("?");
    }
}
