﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switch : MonoBehaviour {
    [SerializeField]
    public GameObject marker;
    public Text titleText;
    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public int lengthOfLineRenderer = 1;
    public Measurement measurement;
    private SwitchTracker switchTracker;
    public VisualFeedback visualFeedback;

    // Use this for initialization
    void Start () {
        switchTracker = GameObject.Find("SwitchManager").GetComponent<SwitchTracker>();
        visualFeedback = GetComponent<VisualFeedback>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnEnable()
    {
        //GameObject.Find("Switches").GetComponent<AlignSwitches>().UpdateLine(transform);
        GetMeasurement();
        this.gameObject.transform.rotation = GameObject.Find("Switches").GetComponent<AlignSwitches>().canonicalSwitch.transform.rotation;
        
    }

    public void ToggleMarker()
    {
        marker.GetComponent<DBSwitchTrackableEventHandler>().enabled = !marker.GetComponent<DBSwitchTrackableEventHandler>().isActiveAndEnabled;
    }

    public void GetMeasurement()
    {
        if(switchTracker == default(SwitchTracker))
        {
            Start();
        }
        this.measurement = switchTracker.GetMeasurementByDistance(this, Camera.main.gameObject.transform.position);
        switchTracker.MovePylons(this);
        if (!visualFeedback.isFirstSwitch)
        {
            this.visualFeedback.nextTie = this.switchTracker.lastSwitch;
        }
        
        this.titleText.text = this.measurement._name;
        
    }
}
