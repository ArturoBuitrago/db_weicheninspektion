﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pylons : MonoBehaviour {

    public Text textNext;
    public Text textNext2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateTexts(SwitchTracker switchTracker, Switch currentSwitch)
    {
        string currentSwitchName = currentSwitch.measurement._name;
        int index = Array.IndexOf(switchTracker.switchNames, currentSwitchName);
        if(switchTracker.switchNames.Length > index + 1)
        {
            textNext.text = switchTracker.switchNames[index + 1];
        }
        else
        {
            textNext.text = "";
        }
        if (switchTracker.switchNames.Length > index + 2)
        {
            textNext2.text = switchTracker.switchNames[index + 2];
        }
        else
        {
            textNext2.text = "";
        }
    }
}
