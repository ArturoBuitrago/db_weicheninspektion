﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignSwitches : MonoBehaviour {

    [SerializeField]
    public GameObject canonicalSwitch;

    List<Transform> ties = new List<Transform>();

    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public int lengthOfLineRenderer = 20;

    private GameObject pylons;


    // Use this for initialization
    void Start () {
        int i = 1;
        int j = 0;
        pylons = GameObject.Find("Pylons");
        //LineRenderer lineRenderer = new LineRenderer();

        /*LineRenderer lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.widthMultiplier = 0.01f;
        lineRenderer.positionCount = 6;

        // A simple 2 color gradient with a fixed alpha of 1.0f.
        float alpha = 1.0f;

        lineRenderer.startColor = c2;
        lineRenderer.endColor = c2;*/

        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateLine(Transform parent)
    {
        foreach(Transform child in parent)
        {
            ties.Add(child);
        }
        lengthOfLineRenderer = ties.Count;
        GetComponent<LineRenderer>().positionCount = ties.Count;
        int i = 0;
        foreach(Transform tie in ties)
        {
            GetComponent<LineRenderer>().SetPosition(i, tie.position);
            i++;
        }
    }

    public void AlignHeight()
    {
        //Debug.Log("[AlignSwitches] AlignHeight");
        foreach (Transform child in transform)
        {
            if (canonicalSwitch != null)
            {
                Vector3 newPos = new Vector3(child.position.x, canonicalSwitch.transform.position.y, child.position.z);
                child.position = newPos;
                pylons.transform.position = new Vector3(pylons.transform.position.x, canonicalSwitch.transform.position.y, pylons.transform.position.z);


            }
            
        }
    }
}
