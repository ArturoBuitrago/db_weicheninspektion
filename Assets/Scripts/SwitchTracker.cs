﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Needs to have all of them enumerated, check length again. 
public enum CurrentMeasurement
{
    sa,
    s1,
    s2,
    s3,
    s4,
    s5,
    s6
};

public class SwitchTracker : MonoBehaviour {



    public Vector3 initialPosition;
    private bool initialPositionSet = false;
    public CurrentMeasurement currentMeasurement = CurrentMeasurement.sa;
    public Dictionary<string, Measurement> measurements = new Dictionary<string, Measurement>();
    public string[] switchNames = { "sa", "s1", "s2", "s3", "s4", "s5", "s6", "s7"};
    public GameObject lastSwitch;
    private GameObject pylons;

    // Use this for initialization
    void Start () {
        //var values = CurrentMeasurement.GetValues(typeof(CurrentMeasurement));
        pylons = GameObject.Find("Pylons");
        float i = 0f;
        foreach(string value in switchNames)
        {
            Measurement ms = new Measurement(value);
            ms.canonicalDistanceToOrigin = i;
            measurements.Add(value, ms);
            i += 1.89f;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InitializeMeasurement(Vector3 currentPosition)
    {
        Debug.Log("[SwitchTracker] Initializing measurements...");
        Debug.Log("[SwitchTracker] Origin: " + currentPosition.ToString());
        this.initialPosition = currentPosition;
        this.measurements["sa"].currentPosition = currentPosition;
        this.measurements["sa"].canonicalDistanceToOrigin = 0f;

        foreach (Measurement measurement in this.measurements.Values)
        {
            measurement.origin = currentPosition;
        }
        initialPositionSet = true;
    }

    public Measurement GetMeasurementByDistance(Vector3 currentPosition)
    {
        foreach (Measurement measurement in this.measurements.Values)
        {
            if (measurement.CheckIfCurrentMeasurement(currentPosition))
            {
                Debug.Log(measurement._name + " was detected");
                return measurement;
            }
        }

        throw new Exception("Measurement not found!");
    }

    public Measurement GetMeasurementByDistance(Switch _switch, Vector3 currentPosition)
    {
        this.lastSwitch = _switch.gameObject;
        Debug.Log("[SwitchTracker] Getting Measurement by Distance :" + currentPosition.ToString());
        if(!initialPositionSet)
        {
            InitializeMeasurement(currentPosition);
            _switch.visualFeedback.isFirstSwitch = true;
            GameObject.Find("Switches").GetComponent<AlignSwitches>().canonicalSwitch = _switch.gameObject;
        }
        foreach (Measurement measurement in this.measurements.Values)
        {
            if (measurement.CheckIfCurrentMeasurement(currentPosition))
            {
                Debug.Log(measurement._name + " was detected");
                return measurement;
            }
        }

        throw new Exception("Measurement not found!");
    }

    public void MovePylons(Switch _switch)
    {
        foreach(Transform child in pylons.transform)
        {
            child.gameObject.SetActive(true);
        }
        pylons.transform.position = _switch.transform.position;
        pylons.GetComponent<Pylons>().UpdateTexts(this, _switch);
    }

}
