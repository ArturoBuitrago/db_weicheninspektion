﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using UnityEngine.Events;

/// <summary>
/// Addition to regular Button
/// Handles the OnInputClicked from the HoloToolkit
/// </summary>

public class HoloButton : MonoBehaviour, IInputClickHandler
{
    public UnityEvent buttonEvent;

    void Start()
    {
        if (buttonEvent == null)
        {
            buttonEvent = new UnityEvent();
        }
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        if (buttonEvent != null)
        {
            buttonEvent.Invoke();
        }
    }
}
