﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Measurement : MonoBehaviour {

    public string _name;
    public float canonicalDistanceToOrigin;
    private Measurement nextMeasurement;
    private Measurement previousMeasurement;
    public Vector3 currentPosition;
    public Vector3 origin;
    private float distanceToOrigin;
    private float admissibleRadius = 1.7f;
    public bool alreadyInstantiated = false;

    public Measurement(string _name)
    {
        this._name = _name;
    }

    public Measurement(string _name, Vector3 currentPosition)
    {
        this._name = _name;
        this.currentPosition = currentPosition;
    }

    public Measurement(string _name, Measurement originalMeasurement)
    {
        this._name = _name;
        if(currentPosition == default(Vector3)){}
        else
        {
            CalculateDistanceToOrigin(originalMeasurement);
        }
        
    }

    public void CalculateDistanceToOrigin(Measurement originalMeasurement)
    {
        distanceToOrigin = Vector3.Distance(originalMeasurement.currentPosition, this.currentPosition);
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    internal bool CheckIfCurrentMeasurement(Vector3 currentPosition)
    {
        float currentDistanceToOrigin = Vector3.Distance(currentPosition, origin);
        
        if (currentDistanceToOrigin > canonicalDistanceToOrigin - admissibleRadius && currentDistanceToOrigin < canonicalDistanceToOrigin + admissibleRadius)
        {
            Debug.Log("[Measurement : " + this._name + "] - " + currentDistanceToOrigin.ToString());
            return true;
        }
        else
        {
            return false;
        }

    }
}
