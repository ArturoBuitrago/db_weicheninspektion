/*==============================================================================
Copyright (c) 2017 PTC Inc. All Rights Reserved.

Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using HoloToolkit.Unity.InputModule;
using UnityEngine;
using Vuforia;
using Viscopic.Holograms;

/// <summary>
///     A custom handler that implements the ITrackableEventHandler interface.
/// </summary>
public class DBSwitchTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{

    public GameObject SwitchGroup;
    private AlignSwitches alignSwitches;

    private bool instantiatedOnce = false;
    public bool trackingEnabled = false;

    private Vector3 hitPosition;
    private float distance;
    private float minDistanceForNextMarker = 0.5f;
    private GazeManager gazeManager;
    private GameObject instantiatedSwitch;

    private Quaternion rotation;
    private int counter = 0;

    [SerializeField]
    public bool secondaryTrackable = false;

    private SwitchTracker switchTracker;

    #region PRIVATE_MEMBER_VARIABLES

    protected TrackableBehaviour mTrackableBehaviour;

    #endregion // PRIVATE_MEMBER_VARIABLES

    #region UNTIY_MONOBEHAVIOUR_METHODS

    protected virtual void Start()
    {
        gazeManager = GameObject.Find("InputManager").GetComponent<GazeManager>();
        alignSwitches = GameObject.Find("Switches").GetComponent<AlignSwitches>();
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        switchTracker = GameObject.Find("SwitchManager").GetComponent<SwitchTracker>();
    }

    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NOT_FOUND)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PRIVATE_METHODS



    protected virtual void OnTrackingFound()
    {
        Debug.Log("Tracking found!");
        /*var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Enable rendering:
        foreach (var component in rendererComponents)
            component.enabled = true;

        // Enable colliders:
        foreach (var component in colliderComponents)
            component.enabled = true;

        // Enable canvas':
        foreach (var component in canvasComponents)
            component.enabled = true;*/
        if (trackingEnabled)
        {
            //ShowSwitchGroup();

            //if (!instantiatedOnce)
            //{
            Debug.Log("Tracking enabled too.");
            //instantiateSwitch();
            //Measurement measurement = switchTracker.GetMeasurementByDistance(Camera.main.transform.position);
            /*if (!measurement.alreadyInstantiated)
            {
                instantiateSwitch(measurement);
                Debug.Log("Instantiate measurement!");
                instantiatedSwitch = Instantiate(this.SwitchGroup, this.gameObject.transform.position, this.gameObject.transform.rotation, GameObject.Find("Switches").transform);
                instantiatedSwitch.SetActive(true);
                measurement.alreadyInstantiated = true;
                instantiatedOnce = true;
                trackingEnabled = false;
                GameObject.Find("Enabler").GetComponent<RepeatableFocusEnabler>().SetActivated();
            }
            else
            {
                Debug.Log("Already Instantiated!");
            }*/
            
            //}
        }
        
    }

    public void instantiateSwitch(Transform tr)
    {
        Measurement measurement = switchTracker.GetMeasurementByDistance(tr.transform.position);
        if (!measurement.alreadyInstantiated)
            {
                
                Debug.Log("Instantiate measurement!");
                //instantiatedSwitch = Instantiate(this.SwitchGroup, this.gameObject.transform.position, this.gameObject.transform.rotation, GameObject.Find("Switches").transform);
                instantiatedSwitch = Instantiate(this.SwitchGroup, tr.position, this.gameObject.transform.rotation, GameObject.Find("Switches").transform);
                instantiatedSwitch.SetActive(true);
                measurement.alreadyInstantiated = true;
                instantiatedOnce = true;
                trackingEnabled = false;
                GameObject.Find("Enabler").GetComponent<RepeatableFocusEnabler>().SetActivated();
            }
            else
            {
                Debug.Log("Already Instantiated!");
            }
    }

    private void ShowSwitchGroup()
    {
        Debug.Log("[AlignSwitches] ShowSwitchGroup");
        if (!this.SwitchGroup.activeSelf)
        {
            this.SwitchGroup.SetActive(true);
        }
        this.SwitchGroup.transform.position = this.transform.position;
        Quaternion newRot = new Quaternion();
        newRot = Quaternion.Euler(0, this.transform.rotation.eulerAngles.y, 0);
        //Debug.Log(this.transform.rotation);
        //Debug.Log(newRot);
        this.SwitchGroup.transform.rotation = newRot;
        //this.enabled = false;
        
    }

    private void HideSwitchGroup()
    {
        this.SwitchGroup.SetActive(false);
    }

    protected virtual void OnTrackingLost()
    {
        /*var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = false;

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;*/
        //this.SwitchGroup1.SetActive(false);
    }

    public void MarkerClicked()
    {
        Debug.Log("CLICK");
        Destroy(this.instantiatedSwitch);
        this.instantiatedSwitch = null;
        this.instantiatedOnce = false;
        //HideSwitchGroup();

    }

    private void Update()
    {
        /*hitPosition = gazeManager.HitPosition;
        //Every frame
        if (secondaryTrackable)
        {
            distance = Vector3.Distance(hitPosition, b: gameObject.transform.position);
            if (distance < minDistanceForNextMarker)
            {
                //this.instantiatedSwitch = null;
                instantiatedOnce = false;
            }
        }
        */
        counter++;
        if(counter == 120)
        {
            counter = 0;
            this.alignSwitches.AlignHeight();
        }
    }

    public void ToggleActive()
    {
        this.gameObject.SetActive(!gameObject.activeInHierarchy);
    }

    #endregion // PRIVATE_METHODS
}
