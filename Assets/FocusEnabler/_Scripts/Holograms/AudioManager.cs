using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : ToolboxSingleton<AudioManager>
{

    public List<ClipType> clipPairs = new List<ClipType>();

    public AudioSource uiSource;
    private AudioSource defaultAudioSource;

    private Dictionary<DefaultClips, AudioClip> clips = new Dictionary<DefaultClips, AudioClip>();

    [Serializable]
    public struct ClipType
    {
        public DefaultClips type;
        public AudioClip clip;
    }

    public enum DefaultClips
    {
        Click,
        Success,
        Hover,
        Start
    }


    private void Start()
    {
        defaultAudioSource = GetComponent<AudioSource>();

        foreach (ClipType pair in clipPairs)
        {
            clips.Add(pair.type, pair.clip);
        }

        SceneManager.sceneLoaded += FindUISource;

    }

    private void FindUISource(Scene arg0, LoadSceneMode arg1)
    {
        //var UI = FindObjectOfType<UICanvasFitter>();
        //if (UI)
            //uiSource = UI.GetComponent<AudioSource>();
    }

    public void PlayClip(DefaultClips clipType, bool useDefaultSource = true)
    {
        AudioClip clip;
        if (clips.TryGetValue(clipType, out clip))
        {
            if (useDefaultSource)
            {
                defaultAudioSource.PlayOneShot(clip);
            }
            else
            {
                //if (uiSource)
                    //  uiSource.PlayOneShot(clip);
            }
        }
    }

}
