﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Viscopic.Holograms
{
    [CreateAssetMenu]
    public class ButtonProfile : ScriptableObject
    {
        public Color Disabled;
        public Color Interactive;
        public Color Focused;
        public Color Activated;
        public Color DisabledActivated;
    }
}