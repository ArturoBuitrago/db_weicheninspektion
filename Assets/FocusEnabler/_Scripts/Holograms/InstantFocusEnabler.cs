﻿using Viscopic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Viscopic.Holograms
{
    public class InstantFocusEnabler : FocusEnabler
    {

        private bool focused = false;

        // Use this for initialization
        void Start()
        {

        }

        public override void OnFocusEnter()
        {
            if (!focused && allowInteraction && this.buttonState == ButtonState.Interactive)
            {
                AudioManager.Instance.PlayClip(onClickClip);
                SetActivated();
                InvokeOnFocusedCall();
                focused = true;
            }
        }

        public override void SetActivated()
        {
            if (this.buttonState == ButtonState.Activated)
                return;

            CancelCoroutine();

            if (this.gameObject.activeInHierarchy)
            {
                coroutine = StartCoroutine(MyColorTween.FadeFromTo(SetCenterColor,
                    GetColor(ButtonState.Activated), centerImage.color, fadeActiveTime, () => coroutine = null));

                //FadeOutProgressbar();
            }

            SetState(ButtonState.Activated);
        }

        public override void OnFocusExit()
        {
            base.OnFocusExit();

            if (allowInteraction)
            {
                focused = false;
                SetInteractive();
            }
        }
    }
}