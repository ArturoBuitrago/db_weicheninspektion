﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Viscopic.Holograms
{
    public class RepeatableFocusEnabler : FocusEnabler
    {
        DBSwitchTrackableEventHandler DBS;
        // Use this for initialization
        void Start()
        {
            DBS = GameObject.Find("ImageTarget_Switch").GetComponent<DBSwitchTrackableEventHandler>();
        }

        public override void SetActivated()
        {
            StartCoroutine(SetInteractiveAfterActivation());
        }

        private IEnumerator SetInteractiveAfterActivation()
        {
            

            DBS.instantiateSwitch(this.transform);

            base.SetActivated();
            yield return new WaitForSeconds(fadeActiveTime);
            yield return new WaitForSeconds(7.0f);
            
            

            base.SetInteractive();
        }
    }
}